describe('Color', function() {
  beforeEach(function() {
  });

  it('should convert to random color', function () {
    var colorAsHex = new Color().toString();
    var anotherColorAsHex = new Color().toString();

    expect(colorAsHex).not.toEqual(anotherColorAsHex);
  });

  it('should convert black to hex value', function () {
    var black = new Color();
    black.red = 0;
    black.green = 0;
    black.blue = 0;

    expect(black.toString()).toEqual('#000000');
  });

  it('should convert white to hex value', function () {
    var white = new Color();
    white.red = 255;
    white.green = 255;
    white.blue = 255;

    expect(white.toString()).toEqual('#FFFFFF');
  });
});
