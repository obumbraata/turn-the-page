describe('string', function() {
  beforeEach(function() {
  });

  it('should pad a string with one character', function () {
    expect(string.prepend('A', 2, '0')).toEqual('0A');
  });

  it('should pad with two characters', function () {
    expect(string.prepend('', 2, '0')).toEqual('00');
  });

  it('should not add any padding', function () {
    expect(string.prepend('AA', 2, '0')).toEqual('AA');
  });

  it('should not modify a string that is already too long', function () {
    expect(string.prepend('AAA', 2, '0')).toEqual('AAA');
  });
});
