set dest=dist

REM Clean out old build files.
if exist rmdir /s %dest%

REM Package into zip.
mkdir %dest%
cd src
7z a ..\%dest%\turn-the-page.xpi * -xr!.git
cd ..

REM Deploy to the browser. Requires the "Extension Auto-Installer" browser plugin.
wget --post-file %dest%\turn-the-page.xpi http://localhost:8888
