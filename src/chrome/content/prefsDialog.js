Components.utils.import("resource://gre/modules/Log.jsm");
Components.utils.import("resource://turnthepage/browser.js");
Components.utils.import("resource://turnthepage/preferences.js");

let log = Log.repository.getLogger("TurnThePage.PrefsDialog");
log.level = Log.Level.Debug;
log.addAppender(new Log.ConsoleAppender(new Log.BasicFormatter()));

function init() {
  log.debug("Initializing preferences.");

  var startupOption = document.getElementById('startup-option');

  if (preferences.rememberLastLocation())
    startupOption.selectedIndex = 0;
  else
    startupOption.selectedIndex = 1;

  addChaptersToDialog();
  addHandlerForOkButton();

  addSaveHandler();
}

function addSaveHandler() {
  var okButton = document.getElementById('ok-button');
  okButton.addEventListener('click', save, false);
}

function save() {
  var rememberOption = document.getElementById('startup-remember-last-location');
  log.debug("Saving " + rememberOption.selected);
  preferences.setRememberLastLocation(rememberOption.selected);
}

function updateStartupAction(e) {
  log.debug("You clickled " + e.source.value);
}

function addChaptersToDialog() {
  var chapters = browser.getChapters();

  log.debug(chapters.length + " chapters found.");

  var rows = document.getElementById('color-list');

  for (var i = 0; i < chapters.length; i++) {
    rows.appendChild(createRowForChapter(chapters[i]));
  }
}

function createRowForChapter(chapter) {
  const XUL_NS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
  var row = document.createElementNS(XUL_NS, "richlistitem");

  var picker = document.createElementNS(XUL_NS, 'colorpicker');
  picker.id = chapter.getId();
  picker.setAttribute('type', 'button');
  picker.setAttribute('color', chapter.getColor());

  picker.addEventListener('change', (function(chapter) {
    return function(event) {
      chapter.setColor(event.target.color);
    }})(chapter)
  );

  var description = document.createElementNS(XUL_NS, 'description');
  description.textContent = chapter.name;

  row.appendChild(picker);
  row.appendChild(description);

  return row;
}

function addHandlerForOkButton() {
  var okButton = document.getElementById('ok-button');
  okButton.addEventListener('click', closePreferences, false);
}

function closePreferences() {
  window.close();
}

window.addEventListener("load", init, false);
