Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("resource://gre/modules/Log.jsm");
Components.utils.import("resource://turnthepage/browser.js");
Components.utils.import("resource://turnthepage/preferences.js");

let log = Log.repository.getLogger("TurnThePage.BrowserOverlay");
log.level = Log.Level.Debug;
log.addAppender(new Log.ConsoleAppender(new Log.BasicFormatter()));

var abHere2 = {
  bookmarks: null,
  lastShownBookmark: null,
  chapter: 'All Chapters',
  prefs: null,

	getInsertionPoint: function(aNode, forceInsideFolder) {
		var ip;
		if (aNode) {
			var isContainer = PlacesUtils.nodeIsFolder(aNode) || PlacesUtils.nodeIsQuery(aNode);
			if (isContainer && forceInsideFolder) {
				ip = { node: aNode,
						index: (abHere2.isInsertTop ? 0 : -1) };
			} else {
				ip = { node: aNode.parent || aNode, //XXX: aNode.parent = null: when right click on bookmarks toolbar space
						index: aNode.bookmarkIndex };
			}
		}
		return ip;
	},

	getInsertionPointDetails: function(target) {
		var ip;
		switch(target.ownerDocument.documentElement.id)
		{
			case "bookmarksPanel":
				var tree = target.ownerDocument.getElementsByTagName("tree")[0];
				ip = abHere2.getInsertionPoint(tree.selectedNode, true);
				if (ip) ip.anchor = getBrowser().selectedBrowser;
				break;

			case "main-window":
				var owner = abHere2.getPopupOwnerElement(target);
				if ((owner.id == "mainPopupSet") && (document.popupNode instanceof XULElement)) {
					// click ABH from context menu
					target = document.popupNode;
					var _insideFolder = Preferences.get("extensions.abhere2.folder.insideFolder", false);
					ip = abHere2.getInsertionPoint(target._placesNode, _insideFolder);
				} else {
					// click ABH from menu
					ip = { node: target._placesNode, index: (abHere2.isInsertTop ? 0 : -1) };
				}
				if (ip) ip.anchor = abHere2.getPopupOwnerElement(target);
				break;
		}
		return ip;
	},

  gotoNextPage: function(event) {
    log.debug("Advancing to next page.");

    abHere2.bookmarks = [];

    var bmsvc = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
                      .getService(Components.interfaces.nsINavBookmarksService);


    var history = Cc["@mozilla.org/browser/nav-history-service;1"]
                  .getService(Ci.nsINavHistoryService);

    // ========================
    var query = history.getNewQuery();
    // query.setFolders([PlacesUtils.bookmarksMenuFolderId], 1);
    query.setFolders([bmsvc.bookmarksMenuFolder], 1);
    // query.searchTerms = "Turn the Page Bookmarks";

    var options = history.getNewQueryOptions();
    // options.queryType = options.QUERY_TYPE_BOOKMARKS;
    options.excludeItems = true;

    var result = history.executeQuery(query, options);

    // The root property of a query result is an object representing the folder you specified above.
    var resultContainerNode = result.root;

    // Open the folder, and iterate over its contents.
    resultContainerNode.containerOpen = true;
    var folderId = null;
    for (var i=0; i < resultContainerNode.childCount; ++i) {
      var childNode = resultContainerNode.getChild(i);

      var isFolder = childNode.type == childNode.RESULT_TYPE_FOLDER;

      if (isFolder && childNode.title === "Turn the Page Bookmarks") {
        log.debug("Found your folder! " + childNode.itemId);
        folderId = childNode.itemId;
      }
    }

    var subfoldersToSearch;

    if (folderId !== null)
      subfoldersToSearch = [folderId];
    else
      subfoldersToSearch = [];

    while (subfoldersToSearch.length > 0) {
      var folderId = subfoldersToSearch.shift();

      log.debug("Creating a query for the bookmarks in our folder");

      query = history.getNewQuery();
      query.setFolders([folderId], 1);

      options = history.getNewQueryOptions();
      options.queryType = options.QUERY_TYPE_BOOKMARKS;

      result = history.executeQuery(query, options);

      // The root property of a query result is an object representing the folder you specified above.
      resultContainerNode = result.root;

      // Open the folder, and iterate over its contents.
      resultContainerNode.containerOpen = true;

      var node = resultContainerNode;

      log.debug("TURN THE PAGE: Looking in folder '" + node.title + " (" + node.childCount + " children).");

      for (var i=0; i < node.childCount; ++i) {
        var childNode = node.getChild(i);

        var isFolder = childNode.type == childNode.RESULT_TYPE_FOLDER;

        if (isFolder) {
          subfoldersToSearch.push(childNode.itemId);
        } else {
          var title = childNode.title;
          var uri = childNode.uri;

          log.debug("TURN THE PAGE: Found bookmark " + title + " @ " + uri);

          // Make sure the page is in the currently viewed chapter.
          if (abHere2.chapter === 'All Chapters') {
            abHere2.bookmarks.push(childNode);
          } else {
            var taggingSvc = Components.classes["@mozilla.org/browser/tagging-service;1"]
                                 .getService(Components.interfaces.nsITaggingService);

            var tags = taggingSvc.getTagsForURI(Services.io.newURI(childNode.uri, null, null));

            if (tags.indexOf(abHere2.chapter) !== -1)
              abHere2.bookmarks.push(childNode);
          }
        }
      }

      log.debug("Done creating list of bookmarks.");
      log.debug("Found " + abHere2.bookmarks.length + " bookmarks.");

      log.debug("Done searching.");
    }

    // Find the next bookmark to show.
    var nextBookmark = null;

    if (abHere2.lastShownBookmark !== null) {
      for (var i = 0; i < abHere2.bookmarks.length - 1; i++) {
        if (abHere2.bookmarks[i].itemId === abHere2.lastShownBookmark.itemId) {
          nextBookmark = abHere2.bookmarks[i+1];
        }
      }
    }

    if (nextBookmark === null && abHere2.bookmarks.length > 0)
      nextBookmark = abHere2.bookmarks[0];

    // Change browser page.
    if (nextBookmark !== null) {
      gBrowser.loadURI(nextBookmark.uri);

      log.debug("Adding " + nextBookmark.uri.toString() + " to preferences");
      preferences.setLastViewedChapter(abHere2.chapter);
      preferences.setLastViewedPage(nextBookmark.uri.toString());

      var siteLabel = document.getElementById("site-name-label");
      siteLabel.value = nextBookmark.title;

      var siteButton = document.getElementById('site-name-button');
      siteButton.setAttribute("tooltiptext", nextBookmark.title);

      abHere2.lastShownBookmark = nextBookmark;
    }
  },

	showChaptersMenu: function(event) {
    log.debug("Populating the menu with something");

    var bmsvc = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
                      .getService(Components.interfaces.nsINavBookmarksService);

    var history = Cc["@mozilla.org/browser/nav-history-service;1"]
                  .getService(Ci.nsINavHistoryService);

    var menu = document.getElementById('mypopup');

    // remove menu items.
    while (menu.firstChild) {
      menu.removeChild(menu.firstChild);
    }

    const XUL_NS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";

    var chapters = browser.getChapters();

    log.debug("Done creating list of chapters.");
    log.debug("Found " + chapters.length + " chapters.");

    // Create all chapters button.
    var item = document.createElementNS(XUL_NS, "menuitem"); // create a new XUL menuitem
    item.setAttribute("label", 'All Chapters');
    item.setAttribute("oncommand", "abHere2.selectChapter(event);");
    menu.appendChild(item);

    var item = document.createElementNS(XUL_NS, "menuseparator"); // create a new XUL menuitem
    menu.appendChild(item);

    for (var i = 0; i < chapters.length; i++) {
      var item = document.createElementNS(XUL_NS, "menuitem"); // create a new XUL menuitem
      item.setAttribute("label", chapters[i].name);
      item.setAttribute("oncommand", "abHere2.selectChapter(event);");
      menu.appendChild(item);
    }
  },

  selectChapter: function(event) {
    log.debug("Selecting a chapter");
    log.debug("Chapter is " + event.target.label);

    var siteNameButton = document.getElementById('site-name-button');
    siteNameButton.classList.remove('chapter-selected');

    var chapters = browser.getChapters();
    for (var i = 0; i < chapters.length; i++) {
      if (chapters[i].name === event.target.label) {
        siteNameButton.classList.add('chapter-selected');
        siteNameButton.style.borderColor = chapters[i].getColor();
      }
    }

    abHere2.chapter = event.target.label;
    preferences.setLastViewedChapter(abHere2.chapter);
  },

	clickBookmarkHere: function(event) {
    // FIND THE TURN THE PAGE BOOKMARKS FOLDER
    var bmsvc = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
                      .getService(Components.interfaces.nsINavBookmarksService);

    var history = Cc["@mozilla.org/browser/nav-history-service;1"]
                  .getService(Ci.nsINavHistoryService);

    // ========================
    var query = history.getNewQuery();
    // query.setFolders([PlacesUtils.bookmarksMenuFolderId], 1);
    query.setFolders([bmsvc.bookmarksMenuFolder], 1);
    // query.searchTerms = "Turn the Page Bookmarks";

    var options = history.getNewQueryOptions();
    // options.queryType = options.QUERY_TYPE_BOOKMARKS;
    options.excludeItems = true;

    var result = history.executeQuery(query, options);

    // The root property of a query result is an object representing the folder you specified above.
    var resultContainerNode = result.root;

    // Open the folder, and iterate over its contents.
    resultContainerNode.containerOpen = true;
    var folderId = null;

    var insertionPointNode = null;

    for (var i=0; i < resultContainerNode.childCount; ++i) {
      var childNode = resultContainerNode.getChild(i);

      var isFolder = (childNode.type === childNode.RESULT_TYPE_FOLDER);

      if (isFolder && childNode.title === "Turn the Page Bookmarks") {
        log.debug("Found your folder! " + childNode.itemId);
        folderId = childNode.itemId;
        insertionPointNode = childNode
      }
    }

    Components.utils.import("resource:///modules/PlacesUIUtils.jsm");

    var ip = null;
    if (insertionPointNode !== null) {
      ip = new InsertionPoint(insertionPointNode.itemId, -1);
    } else {
      // Create the turn the page bookmark folder.
      log.debug("There is no folder for our bookmarks, so I'm creating one.");
      id = PlacesUtils.bookmarks.createFolder(PlacesUtils.bookmarks.bookmarksMenuFolder, "Turn the Page Bookmarks", PlacesUtils.bookmarks.DEFAULT_INDEX);
      log.debug("The folder has been created. It's ID is " + id);

      ip = new InsertionPoint(id, -1);
    }

    var uri = gBrowser.currentURI;
    var title = gBrowser.contentTitle;

    PlacesUIUtils.showBookmarkDialog({
      action: "add",
      type: "bookmark",
      uri: uri,
      title: title,
      defaultInsertionPoint: ip,
      hiddenRows: ['location', 'description', 'keyword', 'loadInSidebar', 'feedLocation', 'siteLocation', 'folderPicker']
    }, window);

    event.stopPropagation();
	},

	createAddBookmarkHereItems: function(target) {
		var node = target._placesNode;
		if (!node) return;
		if (PlacesUtils.nodeIsReadOnly(node)) return;
		if (!PlacesUtils.nodeIsFolder(node) && !PlacesUtils.nodeIsTagQuery(node)) return;

		var abhere = abHere2.get1stElementByAttribute(target, "class", "abhere-menuitem");
		var sprtor = abHere2.get1stElementByAttribute(target, "class", "abhere-separator");

		if (abhere) target.removeChild(abhere);
		else {
			abhere = document.createElement("menuitem");
			abhere.setAttribute("label", abHere2.strBundle.GetStringFromName("label"));
			//abhere.setAttribute("accesskey", abHere2.strBundle.GetStringFromName("accesskey"));
			abhere.setAttribute("class", "abhere-menuitem");
			abhere.setAttribute("onclick", "abHere2.clickBookmarkHere(event);");
			abhere.setAttribute("oncommand", "abHere2.clickBookmarkHere(event);");
		}
		if (sprtor) target.removeChild(sprtor);
		else {
			sprtor = document.createElement("menuseparator");
			sprtor.setAttribute("class", "abhere-separator");
		}

		if (abhere && Preferences.get("extensions.abhere2.misc.showIconic", true)) {
			abhere.classList.add("menuitem-iconic");
		} else {
			abhere.classList.remove("menuitem-iconic");
		}

		var _hideAbhere = (abhere == null) || (abhere && (Preferences.get("extensions.abhere2.position.abhere", 1) == 0));
		if (abhere) abhere.collapsed = _hideAbhere;
		if (sprtor) sprtor.collapsed = _hideAbhere;

		var _top = (Preferences.get("extensions.abhere2.position.abhere", 1) == 1);
		if (sprtor) sprtor.setAttribute("builder", _top ? "start" : "end");
		if (sprtor) target.insertBefore(sprtor, _top ? target.firstChild : null);
		if (abhere) target.insertBefore(abhere, _top ? target.firstChild : null);
		if (abhere) abhere._parentNode = target;
	},

	removeBookmarksForCurrentPage: function() {
		var itemIds = PlacesUtils.getBookmarksForURI(getBrowser().currentURI);
		for (var i = 0; i < itemIds.length; i++) {
			var txn = new PlacesRemoveItemTransaction(itemIds[i]);
			PlacesUtils.transactionManager.doTransaction(txn);
		}
	},

	init: function() {
		window.addEventListener("load", abHere2.onload, false);
    window.addEventListener("unload", abHere2.onUnload, false);
	},

	onload: function() {
    log.debug("loading");


    abHere2.prefs = Components.classes["@mozilla.org/preferences-service;1"]
         .getService(Components.interfaces.nsIPrefService)
         .getBranch("extensions.turnthepage.");

    abHere2.prefs.addObserver("", abHere2, false);

    if (preferences.rememberLastLocation())
      abHere2.chapter = preferences.lastViewedChapter();

    log.debug("Starting with chapter " + abHere2.chapter);

    var chapters = browser.getChapters();
    var siteNameButton = document.getElementById('site-name-button');
    for (var i = 0; i < chapters.length; i++) {
      if (chapters[i].name === abHere2.chapter) {
        siteNameButton.classList.add('chapter-selected');
        siteNameButton.style.borderColor = chapters[i].getColor();
      }
    }

    var lastViewedPage = preferences.lastViewedPage();
    var bookmarks = browser.getBookmarks();

    for (var i = 0; i < bookmarks.length; i++) {
      if (bookmarks[i].uri.toString() === lastViewedPage) {
        log.debug("Starting with page " + bookmarks[i]);
        abHere2.lastShownBookmark = bookmarks[i];
      }
    }

	},

  onUnload: function() {
    log.debug("Unloading");
    abHere2.prefs.removeObserver("", this);
  },

  observe: function(subject, topic, data) {
    log.debug("Observed something");

    if (topic != "nsPref:changed")
      return;
 
    switch(data)
    {
      case "colors.rocksmith":
        break;
    }
  }
}

abHere2.init();
