var EXPORTED_SYMBOLS = ['Color']

Components.utils.import("resource://turnthepage/hex.js");

/**
 * Representation of a color. The constructor creates a new Color object assigned to a random color.
 *
 * @class
 */
var Color = function () {
  this.red = 0;
  this.green = 0;
  this.blue = 0;

  var initialize = function () {
    this.setToRandomColor();
  };

  /**
   * Blue hex value.
   *
   * @return {string}   Hex value as a two-character string.
   */
  this.blueAsHex = function () {
    return Hex.toString(this.blue);
  };

  /**
   * Green hex value.
   *
   * @return {string}   Hex value as a two-character string.
   */
  this.greenAsHex = function () {
    return Hex.toString(this.green);
  };

  /**
   * Red hex value.
   *
   * @return {string}   Hex value as a two-character string.
   */
  this.redAsHex = function () {
    return Hex.toString(this.red);
  };

  /**
   * Assigns this color to a random value.
   */
  this.setToRandomColor = function () {
    this.red = Math.floor(Math.random() * 17);
    this.green = Math.floor(Math.random() * 17);
    this.blue = Math.floor(Math.random() * 17);
  };

  /**
   * Returns a string representation of this color in hex format.
   *
   * @return {string}   Color in hexadecimal format.
   */
  this.toString = function () {
    return '#' + this.redAsHex() + this.greenAsHex() + this.blueAsHex();
  };

  initialize.bind(this)();
};
