var EXPORTED_SYMBOLS = ['Hex'];

Components.utils.import("resource://turnthepage/string.js");

/**
 * Creates a hexadecimal value.
 *
 * @param {number} value    Numeric value.
 */
var Hex = function (value) {
  this.value = value;
};

/**
 * String representations.
 *
 * @param {number} length       Number of desired character in hex value.
 * @param {string} padChar='0'  Output will be padded with this character until it is of desired length.
 *
 * @return {string} hexadecimal value as a string.
 */
Hex.prototype.toString = function (length, padChar) {
  var base = 16;
  var padding = padChar || '0';

  return string.prepend(this.value.toString(base), length, padding).toUpperCase();
};

/**
 * Utility function for converting numbers to hexadecimal format.
 *
 * @param {number} value      Numberic value to convert to hex.
 * @param {number} length=2   Number of desired character in hex value.
 * @param {string} padChar='0'  Output will be padded with this character until it is of desired length.
 *
 * @return {string} hexadecimal value as a string.
 */
Hex.toString = function (value, length, padChar) {
  var defaultLength = length || 2;
  return new Hex(value).toString(defaultLength, padChar);
};
