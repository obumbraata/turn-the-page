var EXPORTED_SYMBOLS = ['extend']

/**
 * Merges the contents of two objects together.
 *
 * @param [Object] target   Object to be merged.
 * @param [Object] source   Object to be merged.
 *
 * @return [Object] New object containing all properties from both target and source.
 */
function extend(target, source) {
  var result = Object.create(target);

  Object.keys(source).map(function (prop) {
      prop in result && (a[prop] = source[prop]);
  });

  return result;
}
