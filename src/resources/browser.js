var EXPORTED_SYMBOLS = ['browser']

Components.utils.import("resource://gre/modules/Services.jsm");
Components.utils.import("resource://turnthepage/chapter.js");
Components.utils.import("resource://gre/modules/Log.jsm");

Components.utils.import("resource://turnthepage/logger.js");

/**
 * Provides an interface for querying aspects of the web browser.
 *
 * @class
 */
var Browser = function () {
  var currentChapter = 'All Chapters';

  var log = new Logger("TurnThePage.browser");

  /**
   * Retrieves the bookmarks that are in the chapter that is currently being viewed.
   *
   * @return Array of bookmark nodes.
   */
  this.getBookmarks = function () {
    var bookmarks = [];

    var bmsvc = Components.classes["@mozilla.org/browser/nav-bookmarks-service;1"]
                      .getService(Components.interfaces.nsINavBookmarksService);

    var history = Components.classes["@mozilla.org/browser/nav-history-service;1"]
                  .getService(Components.interfaces.nsINavHistoryService);

    // ========================
    var query = history.getNewQuery();
    // query.setFolders([PlacesUtils.bookmarksMenuFolderId], 1);
    query.setFolders([bmsvc.bookmarksMenuFolder], 1);
    // query.searchTerms = "Turn the Page Bookmarks";

    var options = history.getNewQueryOptions();
    // options.queryType = options.QUERY_TYPE_BOOKMARKS;
    options.excludeItems = true;

    var result = history.executeQuery(query, options);

    // The root property of a query result is an object representing the folder you specified above.
    var resultContainerNode = result.root;

    // Open the folder, and iterate over its contents.
    resultContainerNode.containerOpen = true;
    var folderId = null;
    for (var i=0; i < resultContainerNode.childCount; ++i) {
      var childNode = resultContainerNode.getChild(i);

      var isFolder = childNode.type == childNode.RESULT_TYPE_FOLDER;

      if (isFolder && childNode.title === "Turn the Page Bookmarks") {
        log.debug("Found your folder! " + childNode.itemId);
        folderId = childNode.itemId;
      }
    }

    var subfoldersToSearch;

    if (folderId !== null)
      subfoldersToSearch = [folderId];
    else
      subfoldersToSearch = [];

    while (subfoldersToSearch.length > 0) {
      var folderId = subfoldersToSearch.shift();

      log.debug("Creating a query for the bookmarks in our folder");

      query = history.getNewQuery();
      query.setFolders([folderId], 1);

      options = history.getNewQueryOptions();
      options.queryType = options.QUERY_TYPE_BOOKMARKS;

      result = history.executeQuery(query, options);

      // The root property of a query result is an object representing the folder you specified above.
      resultContainerNode = result.root;

      // Open the folder, and iterate over its contents.
      resultContainerNode.containerOpen = true;

      var node = resultContainerNode;

      log.debug("TURN THE PAGE: Looking in folder '" + node.title + " (" + node.childCount + " children).");

      for (var i=0; i < node.childCount; ++i) {
        var childNode = node.getChild(i);

        var isFolder = childNode.type == childNode.RESULT_TYPE_FOLDER;

        if (isFolder) {
          subfoldersToSearch.push(childNode.itemId);
        } else {
          var title = childNode.title;
          var uri = childNode.uri;

          log.debug("TURN THE PAGE: Found bookmark " + title + " @ " + uri);

          // Make sure the page is in the currently viewed chapter.
          if (currentChapter === 'All Chapters') {
            bookmarks.push(childNode);
          } else {
            var taggingSvc = Components.classes["@mozilla.org/browser/tagging-service;1"]
                                 .getService(Components.interfaces.nsITaggingService);

            var tags = taggingSvc.getTagsForURI(Services.io.newURI(childNode.uri, null, null));

            if (tags.indexOf(currentChapter) !== -1)
              bookmarks.push(childNode);
          }
        }
      }
    }

    return bookmarks;
  };

  /**
   * Retrieves the Turn the Page chapters.
   *
   * @return {Array<Chapter>}   All Turn the Page chapters sorted alphabetically by name.
   */
  this.getChapters = function () {
    var chapters = [];

    var taggingSvc = Components.classes["@mozilla.org/browser/tagging-service;1"]
                         .getService(Components.interfaces.nsITaggingService);

    var bookmarks = this.getBookmarks();
    for (var i = 0; i < bookmarks.length; i++) {
      var tags = taggingSvc.getTagsForURI(Services.io.newURI(bookmarks[i].uri, null, null));

      for (var j = 0; j < tags.length; j++) {
        var exists = false;
        for (var k = 0; k < chapters.length; k++) {
          if (chapters[k].name === tags[j])
            exists = true;
        }

        if (!exists) {
          var chapter = new Chapter(tags[j]);
          chapters.push(chapter);
        }
      }
    }

    chapters.sort(function(a, b) {
      if (a.name === b.name)
        return 0;

      if (a.name < b.name)
        return -1;
      else
        return 1;
    });

    return chapters;
  };
};

var browser = new Browser();
