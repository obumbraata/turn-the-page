var EXPORTED_SYMBOLS = ['string'];

/**
 * Provides string-related operations.
 */
var string = {
  /**
   * Prepends a character to a string, such that the string is of the desired length.
   *
   * @param {string} text             Text that will be prepended.
   * @param {number} desiredLength    Desired length of the output string.
   * @param {string} prependChar      Character that will be prepended to the string.
   *
   * @example
   * string.prepend('A', 4, '0') == '000A'
   * string.prepend('AAAA', 1, '0') == 'AAAA'
   */
  prepend: function (text, desiredLength, prependChar) {
    var result = text;

    while (result.length < desiredLength) {
      result = prependChar + result;
    }

    return result;
  }
};
