var EXPORTED_SYMBOLS = ['Chapter']

Components.utils.import("resource://turnthepage/util.js");
Components.utils.import("resource://turnthepage/color.js");

var prefs = Components.classes["@mozilla.org/preferences-service;1"]
       .getService(Components.interfaces.nsIPrefService)
       .getBranch("extensions.turnthepage.");

/**
 * Chapter in the Turn the Page book. Chapters are essentially bookmarks, but with additional properties,
 * such as the color assigned to the chapter.
 *
 * @param [string] name                 Name given to this chapter by the user.
 * @param [object] properties           Additional properties assigned to the chapter.
 * @param [object] properties.color     Color given to this chapter. If no color is given, a random
 *                                      color will be assigned to the chapter.
 */
var Chapter = function(name, properties) {
  this.name = name;
  this.color = null;
  this.colorPreferenceName = null;
  this.id = null;

  var defaults = {
    color: null
  };

  var options = extend(defaults, properties || {});

  if (options.color)
    this.setColor(options.color);
};

/**
 * Accesses the color of this chapter.
 *
 * @return [string] Color assigned to this chapter, in the form of #RRGGBB. If no color has been
 *                  assigned to this chapter, this function will attempt to load the chapter's
 *                  color from the stored preferences. If there is no stored color for the chapter, 
 *                  then a random color is assigned to the chapter and returned.
 */
Chapter.prototype.getColor = function() {
  try {
    if (this.color === null)
      this.color = prefs.getCharPref(this.getColorPreferenceName());
  } catch (e) {
    this.setColor(new Color().toString());
  }

  return this.color;
};

/**
 * Accesses the identifier given to the preference setting that stores this chapter's color.
 *
 * @return [string] ID of preference.
 */
Chapter.prototype.getColorPreferenceName = function() {
  if (this.colorPreferenceName === null)
    this.colorPreferenceName = "colors." + this.getId();

  return this.colorPreferenceName;
};

/**
 * Access this chapter's ID. The ID is used when accessing stored preferences relating to this
 * chapter.
 *
 * @return [string] A string value that uniquely identifies this chapter. 
 */
Chapter.prototype.getId = function() {
  if (this.id === null)
    this.id = this.name.replace(/\s/g, '-');

  return this.id;
};

/**
 * Assigns a color to this chapter. The color is saved in the preferences so that it is not lost
 * when the browser is closed.
 *
 * @param [string] newColor     Color being assigned to this chapter in the form of #RRGGBB.
 */
Chapter.prototype.setColor = function(newColor) {
  this.color = newColor;
  prefs.setCharPref(this.getColorPreferenceName(), this.color);
};
