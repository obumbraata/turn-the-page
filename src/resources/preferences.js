var EXPORTED_SYMBOLS = ['preferences']

Components.utils.import("resource://gre/modules/Log.jsm");
let log = Log.repository.getLogger("TurnThePage.preferences");
log.level = Log.Level.Debug;
log.addAppender(new Log.ConsoleAppender(new Log.BasicFormatter()));

var prefs = Components.classes["@mozilla.org/preferences-service;1"]
       .getService(Components.interfaces.nsIPrefService)
       .getBranch("extensions.turnthepage.");

var preferences = {
  rememberLastLocationKey: 'rememberLastLocation',
  lastViewedChapterKey: 'lastViewedChapter',
  lastViewedPageKey: 'lastViewedPage',

  rememberLastLocation: function() {
    try {
      return prefs.getBoolPref(preferences.rememberLastLocationKey);
    } catch (ex) {
      preferences.setRememberLastLocation(true);
      return true;
    }
  },

  setRememberLastLocation: function(isOn) {
    prefs.setBoolPref(preferences.rememberLastLocationKey, isOn);
  },

  lastViewedChapter: function() {
    try {
      return prefs.getCharPref(preferences.lastViewedChapterKey);
    } catch (ex) {
      return 'All Chapters';
    }
  },

  lastViewedPage: function() {
    try {
      return prefs.getCharPref(preferences.lastViewedPageKey);
    } catch (ex) {
      return null;
    }
  },

  setLastViewedChapter: function(chapterName) {
    prefs.setCharPref(preferences.lastViewedChapterKey, chapterName);
  },

  setLastViewedPage: function(url) {
    prefs.setCharPref(preferences.lastViewedPageKey, url);
  }
}
