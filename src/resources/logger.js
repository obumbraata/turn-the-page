var EXPORTED_SYMBOLS = ['Logger']

Components.utils.import("resource://gre/modules/Log.jsm");

/**
 * Logs messages to the Firefox browser console.
 *
 * @param {string} name   Identifies the module that created the logger instance. Useful for identifying
 *                        the origin of the log messages.
 *
 * @class
 */
Logger = function (name) {
  var log = null;

  var initialize = function (name) {
    log = Log.repository.getLogger(name);
    log.level = Log.Level.Debug;
    log.addAppender(new Log.ConsoleAppender(new Log.BasicFormatter()));
  };

  /**
   * Writes a debug message to the log.
   *
   * @param {string} message    The message to be logged.
   */
  this.debug = function (message) {
    log.debug(message);
  };

  initialize(name);
};
