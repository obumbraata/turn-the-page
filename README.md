# Turn the Page

Firefox add-on that lets the user rotate through sites that they visit daily. The add-on
allows you to create a list of sites to be included in the rotation. These are usually sites
that you like to read each day. When you are done with the current page, just click a button to
load the next page in your rotation. 

## Installation

1. Download the file at https://bitbucket.org/obumbraata/turn-the-page/src/62213be589431d6ee6ac9b48f99fb901108a5470/release/turn-the-page.xpi.
2. Drag the downloaded file on top of the Firefox browser window. 
3. You will be prompted to install the plugin. Select yes and restart your browser.

After restarting your browser, you should find the Turn the Page toolbar control on your Firefox toolbar.

## Upgrade

Follow the installation instructions above to upgrade to the latest version. Don't worry, we'll make sure to carry over all of your pages.

## Usage

To add a new page to your book, right-click the website in Firefox and select "Add Page to My Book." Organize your pages into chapters by specify bookmark tags.

## What's New in Version 0.3.2
* Fixed another bug related to colors not being created correctly.

## What's New in Version 0.3.1
* Fixed a bug that could sometimes prevent default chapter colors from being created correctly.

## What's New in Version 0.3

* Option to resume from your last visited page when restarting Firefox.
* Option to assign colors to chapters.
* Chapter colors are shown when a chapter is selected on the toolbar.

## What's New in Version 0.2

* New plugin icons.
* A browser restart is no longer required after adding a page to your book.
* The name of the current page is shown on the browser toolbar.
* You can click the page name in the toolbar to jump to a different chapter.
* The "Add Page to My Book" dialog has been cleaned up -- all of the unused fields are removed.
* You no longer need to specify the "Turn the Page Bookmarks" folder when adding a page to your book. That folder is chosen automatically.
* If you don't have a "Turn the Page Bookmarks" folder -- no worries. It's created for you.

## Removal

Open the Firefox plugin listing and click "Remove" next to the "Turn the Page" name.

## Authors

The idea and design was created by Jared Henry. The software development work was completed by Michael Venable.